/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import java.util.Date;


/**
 *
 * @author User
 */
public class UserDTO {
    private int id;
    private String name;
    private String phone;
    private String address;
    private String public_key;
    private String private_key;
    private String signature;
    private String email;
    private String passphrase;
    private Date dob;
    private String salt;

    
    public int getUserId(){
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public Date getDob() {
        return this.dob;
    }
    
    public void setDob(Date dob){
        this.dob = dob;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address){
        this.address = address;
    }
    
    public String getPhone() {
        return this.phone;
    }
    
    public void setPhone(String phone){
        this.phone = phone;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public String getSignature() {
        return this.signature;
    }
    
    public void setSignature(String signature){
        this.signature = signature;
    }
    
    public String getPublicKey() {
        return this.public_key;
    }
    
    public void setPublicKey(String public_key){
        this.public_key = public_key;
    }
    
    public String getPrivateKey() {
        return this.private_key;
    }
    
    public void setPrivateKey(String private_key){
        this.private_key = private_key;
    }
    
    public String getPassphrase() {
        return this.passphrase;
    }
    
    public void setPassphrase(String passphrase){
        this.passphrase = passphrase;
    }
    
    public String getSalt() {
        return this.salt;
    }
    
    public void setSalt(String salt){
        this.salt = salt;
    }
    
}

