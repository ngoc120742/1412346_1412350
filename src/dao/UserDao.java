/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dto.UserDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import util.MysqlDataAccessHelper;

/**
 *
 * @author User
 */
public class UserDao {
    public static boolean CreateUser(UserDTO user){
        MysqlDataAccessHelper my = new MysqlDataAccessHelper();
      
        my.open();
        boolean result = my.excuteInsertQuery(user);
        my.close();
        return result;
    }
}
