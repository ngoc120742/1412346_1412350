/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import com.mysql.jdbc.Driver;
import com.mysql.jdbc.Statement;
import dto.UserDTO;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cdphuc
 */
public class MysqlDataAccessHelper {

   public Connection conn = null;
   // xu ly ngoai le khi tuong tac voi csdl
   public void displayError(SQLException ex){
       System.out.println(" Error Message:" + ex.getMessage());
       System.out.println(" SQL State:" + ex.getSQLState());
       System.out.println(" Error Code:" + ex.getErrorCode());
   }

   public void open(){// mo ket noi den csdl
       try{
      Driver driver = new org.gjt.mm.mysql.Driver();// nap driver
       DriverManager.registerDriver(driver);// dang ky driver

       String url = "jdbc:mysql://localhost:3306/DATH";
       conn  = DriverManager.getConnection(url, "root", "");//tao ket noi den co so du lieu

       } catch(SQLException ex){// xu ly ngoai le
           displayError(ex);
       }
   }
   public void close(){// dong ket noi co so du lieu
       try {
           if(conn!=null)
               conn.close();
       } catch (SQLException ex) {
           displayError(ex);
       }
   }
   //tao va thuc thi cac cau lenh sql
   // cung cap thong tin trich rut tu csdl va cho phep truy xuat tung dong du lieu
   public ResultSet excuteSelectQuery(String sql){// danh cho cau lenh secect
       ResultSet rs = null;
       try {
           Statement stm = (Statement) conn.createStatement();
           rs = stm.executeQuery(sql);
       } catch (SQLException ex) {
           displayError(ex);
       }
       return rs;
   }
   
   public boolean excuteInsertQuery(UserDTO user){// danh cho cau lenh secect
       String query = " insert into user (email,passphrase,name,address,dob,phone,public_key,private_key,signature)" 
               + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
       
       try {
           PreparedStatement preparedStmt = conn.prepareStatement(query);
           preparedStmt.setString (1, user.getEmail());
           preparedStmt.setString (2, user.getPassphrase());
           preparedStmt.setString (3, user.getName());
           preparedStmt.setString (4, user.getAddress());
           preparedStmt.setString(5, DateUtils.date2String(user.getDob()));
           preparedStmt.setString (6, user.getPhone());
           preparedStmt.setString (7, user.getPublicKey());
           preparedStmt.setString (8, user.getPrivateKey());
           preparedStmt.setString (9, user.getSignature());
           preparedStmt.execute();
       } catch (SQLException ex) {
           displayError(ex);
           return false;
       }
       return true;
   }
   
   public static void main(String args[]) {
       UserDTO user = new UserDTO();
       
       MysqlDataAccessHelper helper = new MysqlDataAccessHelper();
       helper.open();
       
       user.setPassphrase("passphase");
       user.setAddress("address");
       user.setDob(new Date(System.currentTimeMillis()));
       user.setEmail("nghia@vnggg.com");
       user.setName("Nghia");
       user.setPhone("0999");
       user.setPrivateKey("PrivateKey");
       user.setPublicKey("PublicKey");
       user.setSalt("Salt");
       user.setSignature("Sig");
       
       helper.excuteInsertQuery(user);
   }

}
