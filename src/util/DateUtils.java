/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nghiatn
 */
public class DateUtils {

    public static String date2String(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            String newDateString = df.format(date);
            return newDateString;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
}
