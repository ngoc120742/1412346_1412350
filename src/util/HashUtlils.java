/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Random;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author nghiatn
 */
public class HashUtlils {

    public String hash(String plainText) {
        return BCrypt.hashpw(plainText, getNextSalt());
    }

    public boolean verifyHash(String plainText, String hash) {
        return BCrypt.checkpw(plainText, hash);
    }

    public static String getNextSalt() {
        Random RANDOM = new SecureRandom();
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        return new String(salt, StandardCharsets.UTF_8);
    }

}
